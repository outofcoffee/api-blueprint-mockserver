# DEPRECATION NOTICE

**Note:** This project has been superseded by the **[Imposter](https://github.com/outofcoffee/imposter)** project, which provides full Swagger 2.0/OpenAPI REST API, Salesforce, HBase and other mocks.

---

# API Blueprint Mock Server

Java based [API Blueprint](http://github.com/apiaryio/api-blueprint) mock server, with J2SE and Android* support.

## What's it for?
Want to test your app, but don't have connectivity to your mock server?

No problem! Run your mocks on your local machine or Android device*, next to your app.

*\*Android coming soon*

## Writing mocks
We support the [API Blueprint](http://apiblueprint.org/) format e.g.

    # My API
    ## GET /message
    + Response 200 (text/plain)

            Hello World!

For best results, however, please convert your API Blueprints to JSON or YAML AST format first. You can do this using [snowcrash](https://github.com/apiaryio/snowcrash). After conversion they will look like this:

    {
        "_version": "1.0",
        "metadata": {},
        "name": "My API",
        "description": "",
    ...
            "responses": [
                {
                    "name": "200",
                    "description": "",
                    "headers": {
                        "Content-Type": {
                            "value": "text/plain"
                        }
                    },
                    "body": "Hello World!\n",
                    "schema": ""
    ...

## How do I use it?
### Prerequisites
- Java 7 or higher

### Instructions
These instructions assume that you have an API Blueprint AST file named 'MyBlueprint.json'. For an example file, see *src/test/resources/api1.json*

Clone this repository:

    git clone https://bitbucket.org/outofcoffee/api-blueprint-mockserver.git mockserver

_NOTE: If you don't have Git or Subversion, you can also download the repository as a ZIP file._

Change directory to the downloaded repository:

    cd mockserver

Start server by running this script:

    ./mockserver -b MyBlueprint.json -p 9001

_NOTE: If you get a permission error, you might need to make the script executable on your system - first try:_

    chmod +x ./mockserver

**That's it!** Your mock server is up and running. On the command line, you should see:

    Starting server on port 9001
    Endpoints:
    (GET) http://localhost:9001/message
    Press any key to stop...

Test it by visiting [http://localhost:9001/message](http://localhost:9001/message) in your browser.

In your browser, you should see:

    Hello World!

On the command line, you should see:

    Sending HTTP 200 for mock request: /message

You can get documentation for the blueprint just by adding *?blueprint* to the end of an endpoint URL. For example
[http://localhost:9001/message?blueprint](http://localhost:9001/message?blueprint) will show you the API documentation
for the simple API Blueprint.

### Command line options
Other command line arguments are supported:

    ./mockserver [options...]
     --blueprintFile (-b) FILE                                  : Blueprint file
     --blueprintFormat (-f) [MARKDOWN | AST_JSON | AST_YAML]    : Blueprint format
     --host (-h) VAL                                            : Bind host (default = localhost)
     --port (-p) N                                              : Port number (default = any free)
     --pureJava (-j)                                            : Pure Java Blueprint parser (experimental)

## How do I integrate it into my project?
Clone this repository:

    git clone https://bitbucket.org/outofcoffee/api-blueprint-mockserver.git

Build using Gradle wrapper:

    sh ./gradlew clean install

The Mock Server is now available in your local Maven repository ($HOME/.m2/repository). You can depend on it in your projects as follows:

### Maven

    <dependency>
        <groupId>com.gatehill.apibms</groupId>
        <artifactId>api-blueprint-mockserver</artifactId>
        <version>0.1</version>
    </dependency>

### Gradle

    compile 'com.gatehill.apibms:api-blueprint-mockserver:0.1'

### Using the server

Instantiate an instance of *ExecutionServiceImpl* (note: it uses CDI annotations for dependency injection). Here's an example using Guice:

    ExecutionService service = new ExecutionServiceImpl();

    injector = Guice.createInjector(new Module() {
        @Override
        public void configure(Binder binder) {
            binder.bind(MockFactory.class).to(JsonAstMockFactoryImpl.class).in(Singleton.class);
            binder.bind(ServerService.class).to(ServerServiceImpl.class).in(Singleton.class);
        }
    });

    injector.injectMembers(service);

    // use execution service...

For a full working example see *src/test/java/.../integration/EndToEndTest.java*

    // blueprint file
    final File jsonAst = new File("/path/to/api-blueprint-ast.json");

    // start server from blueprint
    final ExecutionInstance exec = service.execute(jsonAst, MockFactory.BlueprintFormat.AST_JSON, 9001);

    // call endpoint
    RestAssured
            .get("http://localhost:" + exec.getPort() + "/message")
            .then()
            .assertThat()
            .body(equalTo("Hello World!\n"));

    // stop server
    serverService.stop(exec);

For an example AST file see *src/test/resources/api1.json*

## Architecture
We use a number of frameworks and libraries to make this project possible:

- [snowcrash-java-binding](https://bitbucket.org/outofcoffee/snowcrash-java-binding) for API Blueprint parsing
- *undertow.io* for HTTP server
- *Guice* for dependency injection
- *Gson* for JSON deserialisation
- *snakeyaml* for YAML deserialisation
- *args4j* for command line argument parsing
- and many more...

For testing:

- *JUnit* for unit testing
- *Mockito* for mocking
- *RestAssured* for REST API testing

## Continuous Integration
We use [drone.io](https://drone.io/bitbucket.org/outofcoffee/api-blueprint-mockserver)

[![Build Status](https://drone.io/bitbucket.org/outofcoffee/api-blueprint-mockserver/status.png)](https://drone.io/bitbucket.org/outofcoffee/api-blueprint-mockserver/latest)

## Contributing
Please see the [wiki](wiki) for details.
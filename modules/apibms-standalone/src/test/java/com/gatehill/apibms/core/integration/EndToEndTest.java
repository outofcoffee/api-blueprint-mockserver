/*
   Copyright 2014 Pete Cornish

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package com.gatehill.apibms.core.integration;

import com.gatehill.apibms.core.MockServerBootstrap;
import com.gatehill.apibms.core.service.ServerUtil;
import com.jayway.restassured.RestAssured;
import io.undertow.util.StatusCodes;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

/**
 * Tests for {@link com.gatehill.apibms.core.MockServerBootstrap}.
 *
 * @author pete
 */
public class EndToEndTest {
    /**
     * Invoke {@link com.gatehill.apibms.core.MockServerBootstrap#main(String...)} with a simple Blueprint and
     * expect a server to be started.
     *
     * @throws Exception
     */
    @Test
    public void testMockServerBootstrap() throws Exception {
        // blueprint file
        final String jsonAst = EndToEndTest.class.getResource("/api1.json").getPath();

        final Integer port = ServerUtil.getFreePort();

        // start server from blueprint
        MockServerBootstrap.main(
                "--blueprintFile", jsonAst,
                "--port", port.toString()
        );

        // test endpoint
        RestAssured
                .get("http://localhost:" + port + "/message")
                .then()
                .assertThat()
                .statusCode(is(StatusCodes.OK))
                .and()
                .body(equalTo("Hello World!\n"));
    }
}

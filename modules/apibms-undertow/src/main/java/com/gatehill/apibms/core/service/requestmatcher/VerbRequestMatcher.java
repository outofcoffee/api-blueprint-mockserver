/*
   Copyright 2014 Pete Cornish

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package com.gatehill.apibms.core.service.requestmatcher;

import com.gatehill.apibms.core.exception.MethodNotAllowedException;
import com.gatehill.apibms.core.model.RequestDefinition;
import com.gatehill.apibms.core.model.ResourceDefinition;
import io.undertow.server.HttpServerExchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Match on verb.
 */
public class VerbRequestMatcher implements RequestMatcher {
    private static final Logger LOGGER = LoggerFactory.getLogger(VerbRequestMatcher.class);

    @Override
    public List<RequestDefinition> matchRequest(HttpServerExchange exchange, ResourceDefinition endpoint, List<RequestDefinition> candidates) throws MethodNotAllowedException {
        final List<RequestDefinition> matchedByVerb = new ArrayList<>();

        // match on verb
        for (RequestDefinition request : candidates) {
            if (exchange.getRequestMethod().equalToString(request.getVerb())) {
                matchedByVerb.add(request);
            }
        }

        LOGGER.trace("Matched {} candidate request definitions by verb for resource: {}", matchedByVerb.size(), endpoint.getUrl());
        if (0 == matchedByVerb.size()) {
            throw new MethodNotAllowedException("Method " + exchange.getRequestMethod() + " not mapped for resource: " + endpoint.getUrl());
        }

        return matchedByVerb;
    }
}

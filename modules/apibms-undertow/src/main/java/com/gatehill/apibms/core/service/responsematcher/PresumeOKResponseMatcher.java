/*
   Copyright 2014 Pete Cornish

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package com.gatehill.apibms.core.service.responsematcher;

import com.gatehill.apibms.core.model.RequestDefinition;
import com.gatehill.apibms.core.model.ResourceDefinition;
import com.gatehill.apibms.core.model.ResponseDefinition;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.StatusCodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Returns a response if it has a status code of 200, otherwise an empty Map of responses.
 */
public class PresumeOKResponseMatcher implements ResponseMatcher {
    private static final Logger LOGGER = LoggerFactory.getLogger(PresumeOKResponseMatcher.class);

    @Override
    public Map<Integer, ResponseDefinition> matchResponse(HttpServerExchange exchange, ResourceDefinition endpoint, RequestDefinition request, Map<Integer, ResponseDefinition> candidates) {
        final Map<Integer, ResponseDefinition> matched = new LinkedHashMap<>();

        if (candidates.size() > 0) {
            // prefer 200
            if (candidates.containsKey(StatusCodes.OK)) {
                matched.put(StatusCodes.OK, candidates.get(StatusCodes.OK));
            }
        }

        LOGGER.trace("Matched {} candidate response definitions by default response code for resource: {}", matched.size(), endpoint.getUrl());
        return matched;
    }
}

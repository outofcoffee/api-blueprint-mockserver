/*
   Copyright 2014 Pete Cornish

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package com.gatehill.apibms.core.service;

import com.gatehill.apib.parser.model.AstFormat;
import com.gatehill.apib.parser.model.ast.v2.*;
import com.gatehill.apib.parser.service.AstParserService;
import com.gatehill.apib.parser.service.BlueprintParserService;
import com.gatehill.apibms.core.model.MockDefinition;
import com.gatehill.apibms.core.model.RequestDefinition;
import com.gatehill.apibms.core.model.ResourceDefinition;
import com.gatehill.apibms.core.model.ResponseDefinition;
import com.gatehill.apibms.core.service.mockfactory.MockFactory;
import com.gatehill.apibms.core.service.mockfactory.MockFactoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.File;
import java.util.ArrayList;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

/**
 * Tests for {@link MockFactoryImpl}.
 */
public class MockFactoryTest {
    private static final int HTTP_SC_OK = 200;

    @InjectMocks
    private MockFactoryImpl factory;

    @Mock
    private BlueprintParserService blueprintParserService;

    @Mock
    private AstParserService astParserService;

    @Before
    public void before() {
        factory = new MockFactoryImpl();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateMock() throws Exception {
        // test data
        final File blueprint = new File(MockFactoryTest.class.getResource("/api1.json").getPath());

        final AstBlueprint ast = new AstBlueprint();
        ast.setName("My API");

        final AstResourceGroup resourceGroup = new AstResourceGroup();
        ast.setResourceGroups(new ArrayList<AstResourceGroup>());
        ast.getResourceGroups().add(resourceGroup);

        final AstResource resource = new AstResource();
        resourceGroup.setResources(new ArrayList<AstResource>());
        resourceGroup.getResources().add(resource);
        resource.setUriTemplate("/message");

        AstAction action = new AstAction();
        resource.setActions(new ArrayList<AstAction>());
        resource.getActions().add(action);
        action.setMethod("GET");

        AstTransactionExample example = new AstTransactionExample();
        action.setExamples(new ArrayList<AstTransactionExample>());
        action.getExamples().add(example);

        AstResponse astResponse = new AstResponse();
        example.setResponses(new ArrayList<AstResponse>());
        example.getResponses().add(astResponse);
        astResponse.setName("200");
        astResponse.setBody("Hello World!\n");

        // mock behaviour
        when(astParserService.fromAst(any(File.class), any(AstFormat.class)))
                .thenReturn(ast);

        // call
        final MockDefinition actual = factory.createMock(blueprint, MockFactory.BlueprintFormat.AST_JSON);

        // assert
        Assert.assertNotNull(actual);
        Assert.assertEquals("My API", actual.getName());
        Assert.assertEquals(1, actual.getEndpoints().size());

        final ResourceDefinition endpoint1 = actual.getEndpoints().get(0);
        Assert.assertNotNull(endpoint1);
        Assert.assertEquals("/message", endpoint1.getUrl());

        Assert.assertEquals(1, endpoint1.getRequests().size());
        final RequestDefinition request = endpoint1.getRequests().get(0);
        Assert.assertEquals("GET", request.getVerb());

        Assert.assertEquals(1, endpoint1.getResponses().size());
        final ResponseDefinition response = endpoint1.getResponses().get(HTTP_SC_OK);
        Assert.assertEquals(200, response.getCode());
        Assert.assertEquals("Hello World!\n", response.getBody());
    }
}

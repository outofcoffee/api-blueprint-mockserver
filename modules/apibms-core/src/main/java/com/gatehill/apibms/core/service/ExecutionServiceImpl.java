/*
   Copyright 2014 Pete Cornish

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package com.gatehill.apibms.core.service;

import com.gatehill.apibms.core.exception.ServiceException;
import com.gatehill.apibms.core.model.ExecutionInstance;
import com.gatehill.apibms.core.model.MockDefinition;
import com.gatehill.apibms.core.model.MockServer;
import com.gatehill.apibms.core.model.ResourceDefinition;
import com.gatehill.apibms.core.service.mockfactory.MockFactory;

import javax.inject.Inject;
import java.io.File;

/**
 * Wraps the parsing of an API blueprint and creation of a server instance hosting the mock definition.
 *
 * @author pete
 */
public class ExecutionServiceImpl implements ExecutionService {
    @Inject
    private MockFactory mockFactory;

    @Inject
    private ServerService serverService;

    /**
     * {@inheritDoc}
     */
    @Override
    public ExecutionInstance execute(File blueprintFile, MockFactory.BlueprintFormat format) throws ServiceException {
        return execute(blueprintFile, format, 0);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExecutionInstance execute(File blueprintFile, MockFactory.BlueprintFormat format, int port) throws ServiceException {
        return execute(blueprintFile, format, ServerService.HOST_LOCALHOST, port);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExecutionInstance execute(File blueprintFile, MockFactory.BlueprintFormat format, String host, int port) throws ServiceException {
        final MockDefinition definition = mockFactory.createMock(blueprintFile, format);
        final MockServer server = serverService.start(definition, host, port);

        return new ExecutionInstance(server.getHost(), server.getPort(),
                definition.getEndpoints().toArray(new ResourceDefinition[definition.getEndpoints().size()]));
    }
}

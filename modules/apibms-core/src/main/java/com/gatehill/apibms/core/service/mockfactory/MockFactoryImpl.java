/*
   Copyright 2014 Pete Cornish

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package com.gatehill.apibms.core.service.mockfactory;

import com.gatehill.apib.parser.model.AstFormat;
import com.gatehill.apib.parser.model.ParserConfiguration;
import com.gatehill.apib.parser.model.ParsingResult;
import com.gatehill.apib.parser.model.ast.v2.*;
import com.gatehill.apib.parser.service.AstParserService;
import com.gatehill.apib.parser.service.BlueprintParserService;
import com.gatehill.apibms.core.exception.ServiceException;
import com.gatehill.apibms.core.model.MockDefinition;
import com.gatehill.apibms.core.model.RequestDefinition;
import com.gatehill.apibms.core.model.ResourceDefinition;
import com.gatehill.apibms.core.model.ResponseDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Build a {@link com.gatehill.apibms.core.model.MockDefinition} from an
 * {@link com.gatehill.apib.parser.model.ast.v2.AstBlueprint}.
 * <p/>
 * This is the preferred implementation.
 */
public class MockFactoryImpl implements MockFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(MockFactoryImpl.class);

    @Inject
    private BlueprintParserService blueprintParserService;

    @Inject
    private AstParserService astParserService;

    @Override
    public MockDefinition createMock(File blueprintFile, BlueprintFormat format) throws ServiceException {
        final File astFile;
        final AstFormat astFormat;

        if (BlueprintFormat.MARKDOWN.equals(format)) {
            // convert to AST first
            LOGGER.info("Converting Markdown format blueprint '{}' to JSON AST", blueprintFile);

            final ParserConfiguration config = new ParserConfiguration();
            final ParsingResult<File> parsingResult =
                    blueprintParserService.convertToAstFile(config, blueprintFile, AstFormat.JSON);

            astFile = parsingResult.getAst();
            astFormat = AstFormat.JSON;

        } else {
            // already AST
            astFile = blueprintFile;
            switch (format) {
                case AST_YAML:
                    astFormat = AstFormat.YAML;
                    break;

                case AST_JSON:
                    astFormat = AstFormat.JSON;
                    break;

                default:
                    throw new UnsupportedOperationException("Unsupported blueprint format: " + format);
            }
        }

        // deserialize to model
        final AstBlueprint blueprint = (AstBlueprint) astParserService.fromAst(astFile, astFormat);

        // convert to mock
        final MockDefinition mock = toMockDefinition(blueprint);

        // set blueprint for documentation
        mock.setBlueprintFile(blueprintFile);

        return mock;
    }

    /**
     * Build a {@link com.gatehill.apibms.core.model.MockDefinition} from an
     * {@link com.gatehill.apib.parser.model.ast.v2.AstBlueprint}.
     *
     * @param astBlueprint
     * @return
     */
    protected MockDefinition toMockDefinition(AstBlueprint astBlueprint) {
        LOGGER.info("Converting blueprint '{}' model to mock definition", astBlueprint.getName());

        final MockDefinition definition = new MockDefinition();
        definition.setName(astBlueprint.getName());

        for (AstResourceGroup astResourceGroup : astBlueprint.getResourceGroups()) {
            for (AstResource astResource : astResourceGroup.getResources()) {
                // add resource headers
                final Map<String, String> parentHeaders = new HashMap<>();
                parentHeaders.putAll(asMap(astResource.getHeaders()));

                // build resource
                final ResourceDefinition resource = new ResourceDefinition(astResource.getName());
                resource.setName(astResource.getName());
                resource.setUrl(astResource.getUriTemplate());

                for (AstAction astAction : astResource.getActions()) {
                    // add action headers
                    parentHeaders.putAll(asMap(astAction.getHeaders()));

                    for (AstTransactionExample example : astAction.getExamples()) {
                        buildRequests(parentHeaders, resource, astAction, example);
                        buildResponses(parentHeaders, resource, example);
                    }
                }

                definition.addEndpoint(resource);
            }
        }

        LOGGER.debug("AST blueprint parsed: {}", definition);
        return definition;
    }

    /**
     * Build {@link com.gatehill.apibms.core.model.RequestDefinition} and {@link com.gatehill.apibms.core.model.ResponseDefinition}
     * instances from the AST action and example.
     *
     * @param parentHeaders
     * @param resource
     * @param astAction
     * @param example
     */
    private void buildRequests(Map<String, String> parentHeaders, ResourceDefinition resource, AstAction astAction, AstTransactionExample example) {
        if (null != example.getRequests() && example.getRequests().size() > 0) {
            // get all requests
            for (AstRequest astRequest : example.getRequests()) {
                // build request
                final RequestDefinition request = new RequestDefinition();
                resource.getRequests().add(request);
                request.setVerb(astAction.getMethod());

                // set headers for inbound matching / request selection
                final Map<String, String> requestHeaders = new HashMap<>();
                requestHeaders.putAll(parentHeaders);
                requestHeaders.putAll(asMap(astRequest.getHeaders()));
                request.setHeaders(requestHeaders);

                LOGGER.trace("Added request from example: {}", request);
            }

        } else {
            // add a default request if none is specified
            final RequestDefinition request = new RequestDefinition();
            resource.getRequests().add(request);
            request.setVerb(astAction.getMethod());

            // set headers for inbound matching / request selection
            final Map<String, String> requestHeaders = new HashMap<>();
            requestHeaders.putAll(parentHeaders);
            request.setHeaders(requestHeaders);

            LOGGER.trace("Added default request: {}", request);
        }

        LOGGER.debug("Built {} request(s) from example", resource.getRequests().size());
    }

    /**
     * Build all the responses within an {@link com.gatehill.apib.parser.model.ast.v2.AstAction}'s examples.
     *
     * @param example
     * @param parentHeaders
     * @return
     */
    private void buildResponses(Map<String, String> parentHeaders, ResourceDefinition resource, AstTransactionExample example) {
        for (AstResponse astResponse : example.getResponses()) {
            final ResponseDefinition response = new ResponseDefinition();
            response.setCode(Integer.valueOf(astResponse.getName()));
            response.setBody(astResponse.getBody());

            // add headers from parent and response definition
            final Map<String, String> headers = new HashMap<>();
            headers.putAll(parentHeaders);
            headers.putAll(asMap(astResponse.getHeaders()));
            response.setHeaders(headers);

            resource.getResponses().put(response.getCode(), response);
        }

        LOGGER.debug("Built {} response(s) from example", resource.getResponses().size());
        LOGGER.trace("Got all responses: {}", resource.getResponses());
    }

    /**
     * Get a {@link java.util.Map} of {@link com.gatehill.apib.parser.model.ast.v2.AstNameValueType} as map
     * of Strings.
     *
     * @param valueTypeList
     * @return
     */
    private Map<String, String> asMap(List<AstNameValueType> valueTypeList) {
        final Map<String, String> map = new HashMap<>();

        if (null != valueTypeList) {
            for (AstNameValueType astNameValueType : valueTypeList) {
                map.put(astNameValueType.getName(), astNameValueType.getValue());
            }
        }

        return map;
    }
}

/*
   Copyright 2014 Pete Cornish

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package com.gatehill.apibms.core.model;

/**
 * Created with IntelliJ IDEA.
 * User: pete
 * Date: 23/02/2014
 * Time: 14:28
 * To change this template use File | Settings | File Templates.
 */
public class ExecutionInstance extends MockServer {
    private ResourceDefinition[] endpoints;

    public ExecutionInstance(String host, int port, ResourceDefinition[] endpoints) {
        super(host, port);
        this.endpoints = endpoints;
    }

    public ResourceDefinition[] getEndpoints() {
        return endpoints;
    }
}

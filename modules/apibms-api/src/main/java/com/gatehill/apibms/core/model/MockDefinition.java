/*
   Copyright 2014 Pete Cornish

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package com.gatehill.apibms.core.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pete on 15/02/2014.
 */
public class MockDefinition {
    private List<ResourceDefinition> endpoints = new ArrayList<>();
    private String name;
    private File blueprintFile;

    public void addEndpoint(ResourceDefinition endpoint) {
        endpoints.add(endpoint);
    }

    public List<ResourceDefinition> getEndpoints() {
        return endpoints;
    }

    @Override
    public String toString() {
        return "MockDefinition{" +
                "endpoints=" + endpoints +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public File getBlueprintFile() {
        return blueprintFile;
    }

    public void setBlueprintFile(File blueprintFile) {
        this.blueprintFile = blueprintFile;
    }
}

/*
   Copyright 2014 Pete Cornish

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package com.gatehill.apibms.core.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by pete on 15/02/2014.
 */
public class ResourceDefinition {
    private String name;
    private String url;
    private List<RequestDefinition> requests = new ArrayList<>();
    private Map<Integer, ResponseDefinition> responses = new HashMap<>();

    public ResourceDefinition() {

    }

    public ResourceDefinition(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ResourceDefinition{" +
                "name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", requests=" + requests +
                ", responses=" + responses +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<RequestDefinition> getRequests() {
        return requests;
    }

    public void setRequests(List<RequestDefinition> requests) {
        this.requests = requests;
    }

    public Map<Integer, ResponseDefinition> getResponses() {
        return responses;
    }

    public void setResponses(Map<Integer, ResponseDefinition> responses) {
        this.responses = responses;
    }
}

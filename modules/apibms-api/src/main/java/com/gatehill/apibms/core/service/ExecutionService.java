/*
   Copyright 2014 Pete Cornish

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package com.gatehill.apibms.core.service;

import com.gatehill.apibms.core.exception.ServiceException;
import com.gatehill.apibms.core.model.ExecutionInstance;
import com.gatehill.apibms.core.service.mockfactory.MockFactory;

import java.io.File;

/**
 * Wraps the parsing of an API Blueprint and creation of a server instance hosting the mock definition.
 *
 * @author pete
 */
public interface ExecutionService {
    /**
     * Parse an API blueprint and create a server instance listening on a free port hosting the mock definition.
     *
     * @param blueprintFile the API Blueprint file
     * @param format        the format of the API Blueprint
     * @return a wrapper containing the MockDefinition and MockServer
     * @throws ServiceException
     */
    ExecutionInstance execute(File blueprintFile, MockFactory.BlueprintFormat format)
            throws ServiceException;

    /**
     * Parse an API blueprint and create a server instance listening on port <code>port</code> hosting the mock definition.
     *
     * @param blueprintFile the API Blueprint file
     * @param format        the format of the API Blueprint
     * @param port          the port on which to listen for connections
     * @return a wrapper containing the MockDefinition and MockServer
     * @throws ServiceException
     */
    ExecutionInstance execute(File blueprintFile, MockFactory.BlueprintFormat format, int port)
            throws ServiceException;

    /**
     * Parse an API blueprint and create a server instance listening on <code>host</code> on port <code>port</code>
     * hosting the mock definition.
     *
     * @param blueprintFile the API Blueprint file
     * @param format        the format of the API Blueprint
     * @param host          the host to which to bind the server
     * @param port          the port on which to listen for connections
     * @return a wrapper containing the MockDefinition and MockServer
     * @throws ServiceException
     */
    ExecutionInstance execute(File blueprintFile, MockFactory.BlueprintFormat format, String host, int port)
            throws ServiceException;
}
